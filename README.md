# SeMex 2019 data

Knowledge graph used for the experiment of our paper "Interactive Method for Causal Discovery in Knowledge Graphs" submitted to SeMex 2019.

The base was built with queries from DBPedia that selected a small portion about writers and theirs books. It has 6,908 triples.

It is composed of four different classes with some datatype properties:

* http://dbpedia.org/ontology/Country: each country is represented by its **label**.
* http://dbpedia.org/ontology/University: each university is represented by its **Academic Ranking of World Universities** (ARWU), and its **endowment**.
* http://dbpedia.org/ontology/Writer: each writer is represented by his/her **gender**, his/her **genre** and his/her **birth date**.
* http://dbpedia.org/ontology/Book: each book is represented by its **number of pages** and its **release date**.

The object properties are:

* http://dbpedia.org/ontology/hasForStudent (created by reversing http://dbpedia.org/ontology/almaMater)
* http://dbpedia.org/ontology/author
* *http://dbpedia.org/ontology/isCountryOf (created by reversing http://dbpedia.org/ontology/country)

The datatype properties are:
* http://dbpedia.org/ontology/arwuW
* http://dbpedia.org/ontology/endowment
* http://xmlns.com/foaf/0.1/gender
* http://dbpedia.org/ontology/genre
* http://dbpedia.org/ontology/birthDate
* http://dbpedia.org/ontology/countryName
* http://dbpedia.org/ontology/releaseDate
* http://dbpedia.org/ontology/numberOfPages
